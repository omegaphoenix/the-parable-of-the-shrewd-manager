## The Parable of the Shrewd Manager

---

### Shrewd

What do you think when you hear someone being described as shrewd?

---

### Shrewd

Would you like to be called shrewd?

---

### Shrewd

Does it have a positive or negative connotation?

---

@img[shadow](assets/img/shrewd-defined.png)

---

### English Translations

@ul[list-spaced-bullets text-09]
- NIV: The Parable of the Shrewd Manager
- ESV: The Parable of the Dishonest Manager
- NASB: The Unrighteous Steward
@ulend

---

@snap[west]
### Background / Context
In Luke 15, Jesus tells the parable of the prodigal son.

How did the younger son treat his inheritance that he demanded from his father?
@snapend

---

@snap[west]
### The Prodigal Son

And not many days later, the younger son gathered everything together and went on a journey into a distant country, and there he **squandered** his estate with loose living.
Luke 15:13
@snapend

---

@snap[west]
### The Older Brother

What was the lesson from the older son's reaction to his brother returning home and whom did he represent?

But he became angry and was not willing to go in; and his father came out and began pleading with him. But he answered and said to his father, ‘Look! For so many years I have been serving you and I have never neglected a command of yours; and yet you have never given me a young goat, so that I might celebrate with my friends;  
Luke 15:28-29
@snapend

---

### Background / Context

Jesus' audience was His disciples and the Pharisees (verse 1 and 14)

---

### Luke 16:1-15
@ul[list-spaced-bullets text-09]
- Parable: Luke 16:1-8a
- Application: Luke 16:8b-15
@ulend

---

@snap[west]
### Luke 16:1
Now He was also saying to the disciples, “There was a rich man who had a manager, and this manager was reported to him as **squandering** his possessions.
@snapend

---

@snap[west]
### Luke 16:2-3
And he called him and said to him, ‘What is this I hear about you? Give an accounting of your management, for you can no longer be manager.’ The manager said to himself, ‘What shall I do, since my master is taking the management away from me? I am not strong enough to dig; I am ashamed to beg.
@snapend

---

What was the reasoning behind the manager's actions?

---

@snap[west]
### Luke 16:4
I know what I shall do, so that when I am removed from the management people will welcome me into their homes.’
@snapend

---

@snap[west]
### Luke 16:8a
And his master praised the unrighteous manager because he had acted shrewdly;
@ul[list-spaced-bullets text-09]
- Why did the master praise him?
- Was what he did right?
@ulend
@snapend

---

@snap[west]
### Application 1
...for the sons of this age are more shrewd in relation to their own kind than the sons of light. And I say to you, make friends for yourselves by means of the wealth of unrighteousness, so that when it fails, they will receive you into the eternal dwellings. - Luke 16:8b-9

@ul[list-spaced-bullets text-08]
- From these two verses, what was Jesus trying to teach?
- What do you observe about how Jesus describes his disciples?
- What did he not say to do with worldly wealth?
- How does this compare with how the manager used worldly wealth?
@ulend
@snapend

---

@snap[west]
### Application 2
“He who is faithful in a very little thing is faithful also in much; and he who is unrighteous in a very little thing is unrighteous also in much. Therefore if you have not been faithful in the use of unrighteous wealth, who will entrust the true riches to you? And if you have not been faithful in the use of that which is another’s, who will give you that which is your own? - Luke 16:10-12

@ul[list-spaced-bullets text-08]
- What are true riches?
- From these verses, what was Jesus teaching?
@ulend
@snapend

---

@snap[west]
### Application 3
No servant can serve two masters; for either he will hate the one and love the other, or else he will be devoted to one and despise the other. You cannot serve God and wealth.” - Luke 16:13

@ul[list-spaced-bullets text-08]
- What is the lesson from this verse?
@ulend
@snapend

---

@snap[west]
### The Pharisees
Now the Pharisees, who were lovers of money, were listening to all these things and were scoffing at Him. And He said to them, “You are those who justify yourselves in the sight of men, but God knows your hearts; for that which is highly esteemed among men is detestable in the sight of God. - Luke 16:14-15

@ul[list-spaced-bullets text-08]
- Why were the Pharisees scoffing at Jesus?
- What did the Pharisees think was God's blessing?
- How do we know that this is false?
@ulend
@snapend

---

Why did Jesus tell this parable to His disciples?

---
